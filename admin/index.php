<?php

include '../connect.php';

    session_start();
    $user = $_SESSION['username'];
    
    if ($user == NULL) {
        header("Location:../home/login.php?notif=tidak_login");
    }
    $cek = mysqli_query($koneksi, "SELECT * FROM data_akun WHERE username='$user'");
    $data = mysqli_fetch_array($cek);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website Universitas Nasional</title>
    <link rel="icon" href="../assets/img/logo_unas.png">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../assets/css/style.css"/>
    <script src="https://kit.fontawesome.com/1b7f3322d3.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- Header -->
    <div class="header">
        <img src="../assets/img/logo_unas.png" alt="logo unas">
        <h2>Universitas Nasional</h2>
        <a href="index.php?page=logout" class="btn btn-danger logout">Log Out &nbsp&nbsp&nbsp<i class="fa fa-sign-out"></i></a>
    </div>    

    <!-- Sidebar -->
    <nav class="row pad-0">
        <div class="sidebar col-sm-3 pad-0 sidebar-collapse">
            <ul>
                <li><h3>Selamat Datang</h3></li>
                <li><center>
                <?php

                    if ($data['foto'] == NULL) {
                        echo "<img src='avatar/avatar_default.png' alt='img_user'>";
                    }
                    else{
                        echo "<img src='avatar/".$data['foto']. "' alt='img_user'>";
                    }
                ?>
                </center></li>
                <li><p><i class="fas fa-user-circle">&nbsp&nbsp&nbsp&nbsp</i><?php echo $data['nama']; ?></p></li>
                <li><a href="index.php?page=home"><i class="fa fa-home">&nbsp&nbsp&nbsp&nbsp</i>Home</a></li>
                    <li class="nav-first-level">
                        <a href="#"><i class="fa fa-chevron-right" aria-hidden="true">&nbsp&nbsp&nbsp&nbsp&nbsp</i>Data Mahasiswa<i class="fas fa-caret-down" id="float-right"></i></a>
                            <ul class="nav-second-level">
                                <li><a href="index.php?page=data_mahasiswa"><i class="fa fa-desktop">&nbsp&nbsp&nbsp&nbsp&nbsp</i>Lihat Data Mahasiswa</a></li>
                                <li><a href="index.php?page=tambah_mahasiswa"><i class="fa fa-plus-square">&nbsp&nbsp&nbsp&nbsp</i>Tambah Data Mahasiswa</a></li>
                            </ul>
                    </li>
                    <li class="nav-first-level">
                        <a href="#"><i class="fa fa-chevron-right" aria-hidden="true">&nbsp&nbsp&nbsp&nbsp&nbsp</i>Data Dosen Pengajar<i class="fas fa-caret-down" id="float-right"></i></a>
                            <ul class="nav-second-level">
                                <li><a href="index.php?page=data_dosen"><i class="fa fa-desktop">&nbsp&nbsp&nbsp&nbsp</i>Lihat Data Dosen</a></li>
                                <li><a href="index.php?page=tambah_dosen"><i class="fa fa-plus-square">&nbsp&nbsp&nbsp&nbsp</i>Tambah Data Dosen</a></li>
                            </ul>
                    </li>
                    <li><a href="index.php?page=pengaturan_akun"><i class="fa fa-cog">&nbsp&nbsp&nbsp&nbsp</i>Pengaturan Akun</a></li>
                <li><a href="index.php?page=logout"><i class="fa fa-sign-out">&nbsp&nbsp&nbsp&nbsp</i>Log Out</a></li>
            </ul>
        </div>

        <!-- Konten -->
        <div class="konten col-sm-9">
                    <?php
                        if(!isset($_GET['page'])){
                            include 'home.php';
                        }
                        else{
                            $page = $_GET['page'];

                            include $page. '.php';
                        }
                    ?>
        </div>
    </nav>

    <!-- Footer -->
    <div class="footer">
        <span>Copyright &copy Rizki Akbar Mahdafiki. All Rights Reserved</span>
    </div>
</body>
</html>